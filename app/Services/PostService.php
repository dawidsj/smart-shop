<?php

namespace App\Services;

use App\Facades\ImageManager;
use App\Http\Requests\EditBlogPostRequest;
use App\Http\Requests\BlogPostRequest;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;


class PostService
{
    public const STORAGE_IMAGES_PATH = 'storage/photos/posts/';
    public const PUBLIC_IMAGES_PATH = 'public/photos/posts/';

    public function store(BlogPostRequest $request)
    {

        $fileName = ImageManager::safeImage(self::PUBLIC_IMAGES_PATH, $request->file('image'));

        return Post::create([
            'topic' => $request->get('topic'),
            'slug' => $request->get('slug'),
            'teaser' => $request->get('teaser'),
            'content' => $request->get('content'),
            'author' => Auth::id(),
            'image' => $fileName,
        ]);
    }

    public function update(BlogPostRequest $request, int $id)
    {
        /** @var Post $post */
        $post = Post::query()->find($id);
        if ($request->exists('image')) {
            ImageManager::removeImage(self::PUBLIC_IMAGES_PATH . $post->image);
            $fileName = ImageManager::safeImage(self::PUBLIC_IMAGES_PATH, $request->file('image'));
            $post->image = $fileName;
        }
        $post->update([
            'topic' => $request->get('topic'),
            'slug' => $request->get('slug'),
            'teaser' => $request->get('teaser'),
            'content' => $request->get('content'),
        ]);
    }
}