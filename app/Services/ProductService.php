<?php

namespace App\Services;

use App\Facades\ImageManager;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\ProductFeatures;
use App\Models\ProductImages;

class ProductService
{
    public const STORAGE_IMAGES_PATH = 'storage/photos/products/';
    public const PUBLIC_IMAGES_PATH = 'public/photos/products/';

    public function store(ProductRequest $request) {
        $product = $this->storeProduct($request);

        $features = $request->get('features');
        $values = $request->get('values');

        if (isset($features, $values)) {
            for ($i = 0; $i < count($features); $i++) {
                $this->storeProductFeature($product, $features[$i], $values[$i]);
            }
        }
    }

    private function storeProduct(ProductRequest $request): Product
    {
        /** @var Product $product */
        $product = Product::query()->create([
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'quantity' => $request->get('quantity'),
            'active' => (int) $request->get('active'),
        ]);

        foreach ($request->file('image') as $index => $image) {
            $fileName = ImageManager::safeImage(self::PUBLIC_IMAGES_PATH, $image);

            ProductImages::query()->create([
                'product_id' => $product->id,
                'name' => $fileName,
                'order' => $index,
            ]);
        }

        return $product;
    }

    private function storeProductFeature(Product $product, int $featureId, $featureValue): void
    {
        ProductFeatures::query()->create([
            'product_id' => $product->id,
            'feature_id' => $featureId,
            'value' => $featureValue,
        ]);
    }

    //Update product
    public function update(ProductRequest $request, int $id)
    {

        $product = $this->updateProduct($request, $id);

        ProductFeatures::query()->where('product_id', $id)->delete();

        $features = $request->get('features');
        $values = $request->get('values');

        if (isset($features, $values)) {
            for ($i = 0; $i < count($features); $i++) {
                $this->storeProductFeature($product, $features[$i], $values[$i]);
            }
        }

        return redirect(route('panel.product.editList'));

    }

    private function updateProduct(ProductRequest $request, int $id): Product
    {
        /** @var Product $product */
        $product = Product::query()->find($id);
        $product->update([
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'quantity' => $request->get('quantity'),
            'active' => (int) $request->get('active'),
        ]);

        if ($request->exists('image')) {
            foreach ($request->file('image') as $index => $image) {
                $fileName = ImageManager::safeImage(self::PUBLIC_IMAGES_PATH, $image);

                ProductImages::query()->create([
                    'product_id' => $product->id,
                    'name' => $fileName,
                    'order' => $index,
                ]);
            }
        }

        return $product;
    }

}