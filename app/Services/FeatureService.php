<?php

namespace App\Services;

use App\Http\Requests\StoreFeatureRequest;
use App\Models\Feature;

class FeatureService
{
    public function store(StoreFeatureRequest $request) {

        return Feature::create([
           'name' => $request->get('name'),
        ]);
    }
}