<?php


namespace App\Services;

use Illuminate\Support\Facades\Auth;

class LoginService
{
    public function login(array $loginData):bool {

        return Auth::attempt($loginData);
    }
}