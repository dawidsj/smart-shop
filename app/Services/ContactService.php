<?php

namespace App\Services;

use App\Http\Requests\ContactRequest;
use App\Models\ContactEmail;

class ContactService
{
    public function store(ContactRequest $request)
    {
        return ContactEmail::query()->create([
           'name' => $request->get('name'),
           'email' => $request->get('email'),
           'topic' => $request->get('topic'),
           'content' => $request->get('msgContent'),
           'copy_to_author' => (int) $request->get('active'),
        ]);
    }
}