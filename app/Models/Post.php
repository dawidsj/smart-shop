<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @property string $topic
 * @property string $slug
 * @property string $teaser
 * @property string $content
 * @property string $image
 * @property int $author
 * @package App\Models
 */
class Post extends Model
{
    use SoftDeletes;

    protected $table = 'posts';

    protected $fillable = [
        'topic',
        'slug',
        'teaser',
        'content',
        'author',
        'image',
    ];

    public function postAuthor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author', 'id');
    }
}
