<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class ProductFeatures
 * @property integer $product_id
 * @property integer $feature_id
 * @property string $value
 * @package App\Models
 */

class ProductFeatures extends Model
{
    protected $table = 'product_features';

    protected $fillable = [
        'product_id',
        'feature_id',
        'value',
    ];

    public function feature(): BelongsTo
    {
        return $this->belongsTo(Feature::class, 'feature_id', 'id');
    }
}
