<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Feature
 * @property string $name
 * @package App\Models
 */

class Feature extends Model
{
    protected $table = 'features';

    public $timestamps = false;

    protected $fillable = [
      'name',
    ];
}
