<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ContactEmail
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $content
 * @property boolean $copy_to_author
 * @package App\Models
 */
class ContactEmail extends Model
{
    use HasFactory;

    protected $table = 'Contact_Emails';

    protected $fillable = [
        'name',
        'email',
        'topic',
        'content',
        'copy_to_author',
    ];

}
