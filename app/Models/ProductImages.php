<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductImages
 * @property integer $product_id
 * @property integer $order
 * @property string $name
 * @package App\Models
 */

class ProductImages extends Model
{
    protected $table = 'product_images';

    protected $fillable = [
        'product_id',
        'order',
        'name',
    ];

    use HasFactory;
}
