<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductRequest
 * @property int $id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property integer $quantity
 * @property bool $active
 * @property string $meta_name
 * @property string $meta_description
 * @property string $slug
 * @package App\Models
 */

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'name',
        'description',
        'price',
        'quantity',
        'active',
        'meta_name',
        'meta_description',
        'slug',
    ];

    public function images(): HasMany
    {
            return $this->hasMany(ProductImages::class, 'product_id', 'id');
    }

    public function features(): HasMany
    {
        return $this->hasMany(ProductFeatures::class, 'product_id', 'id');
    }

}
