<?php

namespace App\Providers;

use App\Facades\ImageManager;
use Illuminate\Support\ServiceProvider;
use App\Helpers\ImageManagerHelper;

class FacadesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ImageManager::class, function () {
           return new ImageManagerHelper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
