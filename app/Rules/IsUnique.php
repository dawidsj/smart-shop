<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Model;

class IsUnique implements Rule
{
    protected Model $model;
    protected string $field;
    protected ?string $id;

    /**
     * IsUnique constructor.
     * @param string $model
     * @param string $field
     * @param string|null $id
     */
    public function __construct(string $model, string $field, string $id = null)
    {
        $this->model = app($model);
        $this->field = $field;
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $data = $this->model->where($this->field, $value);
        if (!is_null($this->id)) {
            $data->where('id', '!=', $this->id);
        }

        return $data->get()->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
       return 'Pole :attribute musi być unikatowe!';
    }
}
