<?php

namespace App\Facades;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Facade;

/**
 * Class ImageManager
 * @package App\Facades
 * @method static safeImage(string $path, UploadedFile $file)
 * @method static removeImage(string $file)
 */
class ImageManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return self::class;
    }
}