<?php

namespace App\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ImageManagerHelper
{
    public function safeImage(string $path, UploadedFile $file): string
    {
        return basename(Storage::putFile($path, $file));
    }

    public function removeImage(string $file): void
    {
        Storage::delete('public/photos/posts/' . $file);
    }
}