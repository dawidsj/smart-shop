<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use App\Models\Feature;
use App\Models\Product;
use App\Services\ProductService;
use Gloudemans\Shoppingcart\Facades\Cart;

class ProductsController extends Controller
{
    private ProductService $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    //Features

    public function create()
    {
        $features = Feature::all();

        return view('panel.product.form', ['features' => $features]);
    }

    //Product

    public function store(ProductRequest $request)
    {
        try {
            $this->productService->store($request);
            return redirect(route('product.list'));

        } catch (\Exception $exception) {
                        dd($exception->getMessage(), $exception->getLine(), $exception->getFile());
            return redirect()->back()->withErrors(['is_error' => 'Błąd']);
        }
    }

    public function list()
    {
        $products = Product::query()
            ->with([
                'images',
                'features.feature',
            ])
            ->where('active', 1)
            ->get();

        return view('product.index', ['products' => $products]);
    }

    public function show(string $slug)
    {
        $product = Product::with(['images', 'features'])->where('slug', $slug)->first();

        if (is_null($product)) {
            return redirect(route('product.list'));
        }

        return view('product.single-product', ['product' => $product]);

    }

    // Update product

    public function remove(int $id)
    {
        Product::query()
            ->with([
                'images',
                'features.feature',
            ])
            ->where('id', $id)
            ->delete();

        return redirect(route('panel.product.editList'));
    }

    public function editList()
    {
        $products = Product::query()
            ->with([
                'images',
                'features.feature',
            ])
            ->get();

        return view('panel.product.list', ['products' => $products]);
    }

    public function edit(int $id)
    {
        $features = Feature::all();
        $product = Product::query()
            ->with([
                'images',
                'features.feature',
            ])
            ->find($id);

        return view('panel.product.form', compact('product', 'features'));
    }

    public function update(ProductRequest $request, int $id)
    {
        try {
            $this->productService->update($request, $id);

            return redirect(route('panel.product.editList'));

        } catch (\Exception $exception) {
            dd($exception->getMessage(), $exception->getLine(), $exception->getFile());
            return redirect()->back()->withErrors(['is_error' => 'Błąd']);
        }

    }
    //Cart
    public function addItemToCart(int $id)
    {
        $product = Product::with(['images', 'features'])->where('id', $id)->first();

        Cart::add([
            'id' => $product->id,
            'name' => $product->name,
            'price' => $product->price,
            'weight' => 1, 'qty' => '1',
            'options' => [
                'image' => $product->images[0]->name,
                'quantity' => $product->quantity,
            ],
        ]);

        return redirect()->back();
    }
    public function updateCart(string $rowId, Request $request)
    {
        $getItem = Cart::get($rowId);

        if ($getItem->options->quantity < $request->quantity) {
            return redirect()->back()->with('error', 'Liczba produktów nie może być większa niż max!');
        }
        Cart::update($rowId, $request->quantity);

        return redirect()->back();
    }
    public function removeItemFromCart(string $rowId)
    {
        Cart::remove($rowId);

        return redirect()->back();
    }

    public function cart()
    {
        $cart = Cart::content()->all();

        return view('product.cart', ['cart' => $cart]);
    }
    public function test():void
    {
        Cart::destroy();
    }


}
