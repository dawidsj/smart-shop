<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFeatureRequest;
use App\Models\Feature;
use App\Services\FeatureService;
use Illuminate\Http\Request;

class FeatureController extends Controller
{
    protected FeatureService $featureService;

    public function __construct(FeatureService $featureService)
    {
        $this->featureService = $featureService;
    }

    public function storeFeature(StoreFeatureRequest $request)
    {
        try {
            $this->featureService->store($request);
            return redirect(route('panel.feature.create'));
        } catch (\Exception $exception) {
            dd($exception->getMessage(), $exception->getLine(), $exception->getFile());
            return redirect()->back()->withErrors(['is_error' => 'Błąd']);
        }

    }

    public function create() {

        $features = Feature::all();

        return view('panel.feature.index', ['features' => $features]);
    }
}
