<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Services\LoginService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected LoginService $loginService;

    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;

    }

    public function login(LoginRequest $request) {

        try {
            if (!$this->loginService->login($request->all(['name', 'password']))) {
                return redirect()->back()->withErrors(['is_error' => 'Błędne dane logowania']);
            }
            return redirect()->back();
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(['is_error' => 'Błąd krytyczny!']);
        }
    }

    public function logout() {
        Auth::logout();

        return redirect('auth');
    }
}
