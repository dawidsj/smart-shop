<?php

namespace App\Http\Controllers;
use App\Http\Requests\EditBlogPostRequest;
use App\Http\Requests\BlogPostRequest;
use App\Models\Post;
use App\Services\PostService;

class BlogPostsController extends Controller
{
    protected PostService $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function store(BlogPostRequest $request)
    {
        try {
            $this->postService->store($request);

            return redirect(route('blog.blog'));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(['is_error' => 'Błąd']);
            /*dd($exception->getMessage(), $exception->getLine(), $exception->getFile());*/
        }
    }

    public function create() {
        return view('panel.blog.form');
    }

    public function renderBlog()
    {
        $posts = Post::with('postAuthor')->orderByDesc('created_at')->get();

        return view('blog.index', ['posts' => $posts]);
    }

    public function show(string $slug)
    {
        $post = Post::with('postAuthor')->where('slug', $slug)->first();
        if (is_null($post)) {
            return redirect(route('blog.blog'));
        }
        return view('blog.post', [
            'post' => $post,
        ]);
    }

    public function listPosts()
    {
        $posts = Post::with('postAuthor')->orderByDesc('created_at')->get();

        return view('panel.blog.list', ['posts' => $posts]);
    }

    public function remove(int $id)
    {
        Post::query()->where('id', $id)->delete();

        return redirect(route('panel.post.editList'));
    }

    public function edit(int $id)
    {
        $post = Post::find($id);

        return view('panel.blog.form', compact('post'));
    }

    public function update(BlogPostRequest $request, int $id)
    {
        try {
            $this->postService->update($request, $id);

            return redirect(route('panel.post.editList'));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(['is_error' => 'Błąd']);
            /*dd($exception->getMessage(), $exception->getLine(), $exception->getFile());*/
        }
    }
}
