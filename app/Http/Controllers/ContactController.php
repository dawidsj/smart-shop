<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;
use App\Services\ContactService;


class ContactController extends Controller
{
    private ContactService $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    public function form()
    {
        return view('contact');
    }

    public function sendContactEmail(ContactRequest $request)
    {
        try {
            $this->contactService->store($request);

            Mail::to(env('MAIL_USERNAME'))->send(new ContactEmail());

            if ($request->active == 1) {
                Mail::to($request->email)->send(new ContactEmail());
            }

            return redirect()->back()->with('success', 'Wiadomość została wysłana poprawnie. Dziękujemy za kontakt.');

        } catch (\Exception $exception) {
            dd($exception->getMessage(), $exception->getLine(), $exception->getFile());
            return redirect()->back()->withErrors(['is_error' => 'Błąd']);
        }

    }


}
