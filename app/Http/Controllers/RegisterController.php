<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Services\RegisterService;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    protected RegisterService $registerService;

    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
    }

    public function register(RegisterRequest $request) {

        try {
            $user = $this->registerService->registerUser($request);
            Auth::login($user);

            return redirect(route('auth'));
        } catch (\Exception $exception) {
            dd($exception->getMessage(),$exception->getLine(), $exception->getFile());
        }

    }
}
