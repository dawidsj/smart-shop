<?php

namespace App\Http\Requests;

use App\Models\Post;
use App\Rules\IsUnique;
use Illuminate\Foundation\Http\FormRequest;

class BlogPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'topic' => ['required', 'min:3', new IsUnique(Post::class, 'topic', $this->get('id'))],
            'content' => 'required|min:3',
            'teaser' => 'required',
            'slug' => ['required', 'string', new IsUnique(Post::class, 'slug', $this->get('id'))],
            'image' => 'required_without:id|image|mimes:jpg,jpeg,png',
        ];
    }
}
