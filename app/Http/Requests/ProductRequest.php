<?php

namespace App\Http\Requests;

use App\Models\Product;
use App\Rules\IsUnique;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','min:3', new IsUnique(Product::class, 'name', $this->get('id'))],
            'slug' => ['required', 'string', new IsUnique(Product::class, 'slug', $this->get('id'))],
            'description' => 'required',
            'price' => 'required',
            'quantity' => 'required',
            'image' => 'required_without:id',
            'image.*' => 'required_without:id|image|mimes:jpg,jpeg,png',
        ];
    }
}
