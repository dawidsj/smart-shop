@extends('layouts.app')

@section('title')
Pozdro
@endsection

@section('content')
<div class="container">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row py-3">
        <div class="info-side-panel col-lg-4">
            Zaloguj się! <br>
            Warto!<br>
        </div>
        <div class="form-side-panel col-lg-8 py-5 px-5">
            <form method="post" action="{{Route('auth.login')}}">
                @csrf
                <div class="form-group">
                    <label for="loginInput">Login</label>
                    <input type="text" name="name" class="form-control" id="loginInput" placeholder="email@przykład.pl">
                </div>
                <div class="form-group">
                    <label for="passwordInput">Hasło</label>
                    <input type="password" name="password" class="form-control" id="passwordInput" placeholder="Min. 3 znaki">
                </div>
                <input type="submit" value="Zaloguj się" class="btn btn-success">
            </form>
        </div>
    </div>

    <hr>

    <div class="row py-3">
        <div class="form-side-panel col-lg-8 py-5 px-5">
            <form method="post" action="{{route('auth.register')}}">
                @csrf
                <div class="form-group">
                    <label for="emailInput">Email</label>
                    <input type="text" name="email" class="form-control" id="emailInput" placeholder="email@przykład.pl">
                </div>
                <div class="form-group">
                    <label for="loginInput">Login</label>
                    <input type="text" name="name" class="form-control" id="loginInput" placeholder="Min. 3 znaki">
                </div>
                <div class="form-group">
                    <label for="passwordInput">Hasło</label>
                    <input type="password" name="password" class="form-control" id="passwordInput" placeholder="Min. 3 znaki">
                </div>
                <input type="submit" value="Zarejestruj się" class="btn btn-success">
            </form>
        </div>
        <div class="info-side-panel col-lg-4">
            Zarejestruj się już teraz! <br>
            Nowe funkcje będą twoje!<br>
        </div>
    </div>
</div>

@endsection