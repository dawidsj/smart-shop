@extends('layouts.app')
@section('css')
@endsection
@section('title')
    Kontakt
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Kontakt</h2>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('success'))
            <div class="col-12 alert alert-success">
                {!! Session::get('success') !!}
            </div>
        @endif
        <div class="row">
            <div class="col-8 offset-2 my-3 py-3 new-post-form">
                <h4 class="text-center">
                        Witaj!<br>
                </h4>
                <span>
                        Jeżeli potrzebujesz się z nami skontaktować z chęcią Ci pomożemy!<br> Wykorzystaj do tego poniższy formularz!
                </span>
                <hr>
                <form action="{{route('contact.sendEmail')}}" method="POST" id="contact-form">
                    @csrf
                    <div class="form-group">
                        <label for="name"><h5>Podaj imię:</h5></label>
                        <input type="text" name="name" class="form-control col-12" id="name" required>
                    </div>
                    <div class="form-group">
                        <label for="email"><h5>Podaj swój email:</h5></label>
                        <input type="email" name="email" class="form-control col-12" id="email" required>
                    </div>
                    <div class="form-group">
                        <label for="topic"><h5>Tytuł wiadomości / problemu:</h5></label>
                        <input type="text" name="topic" class="form-control col-12" id="topic" required>
                    </div>
                    <div class="form-group">
                        <label for="msgContent"><h5>Treść wiadomości:</h5></label>
                        <textarea type="text" name="msgContent" class="form-control col-12" id="editor"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="active" value="1" class="form-check-label" id="active">
                        <label for="active"><h5>Czy chcesz kopię wiadomości na swój email?</h5></label>
                    </div>
                    <input type="submit" value="Submit" class="btn btn-success btn-block" class="g-recaptcha"
                           data-sitekey="reCAPTCHA_site_key"
                           data-callback='onSubmit'
                           data-action='submit'>

                </form>
            </div>
        </div>
        </div>
    </div>
    {!! htmlScriptTagJsApi() !!}
@endsection

@section('js')

    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection