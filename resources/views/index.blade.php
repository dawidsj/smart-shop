@extends('layouts.app')

@section('title')
    Strona główna
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12" align="center">
            <h1>Witaj na stronie :)</h1>
        </div>
    </div>
    <div class="col-12 text-center my-5">
        <h3>Wciąż pracujemy nad tym...</h3>
        <img src="https://img.icons8.com/ios/50/000000/work-light.png" class="my-3"/>
        <h4>Zachęcamy do skorzystania z opcji dostępnych na pasku nawigacji!</h4>
    </div>

</div>

@endsection