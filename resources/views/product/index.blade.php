@extends('layouts.app')
@section('css')
@endsection
@section('title')
    Produkty
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-2 border-right">
                <s>Kategorie:</s>
                <h4>W BUDOWIE</h4>
            </div>

            <div class="col-10 product">
                @foreach($products as $product)
                <a href="{{route('product.show', ['slug' => $product->slug])}}">
                <div class="row">
                    <div class="col-3">
                        <img src="{{asset(\App\Services\ProductService::STORAGE_IMAGES_PATH . $product->images->first()->name)}}" class="img-fluid img-product img-thumbnail" alt="">
                    </div>
                    <div class="col-9">
                        <span style="font-size: 1.15rem">{{$product->name}}<br></span>
                        <span>Dostępna ilość: {{$product->quantity}}<br></span>
                        <span class="align-bottom" style="position: absolute;bottom:0;font-size: 1.15rem"><b>{{$product->price}}</b> zł (brutto)<br></span>
                        <form action="{{route('product.addItemToCart', $product->id)}}" method="POST">
                            @csrf
                            <input type="submit" value="Dodaj do koszyka" class="btn btn-success" class="align-right" style="position: absolute;right: 0; bottom: 0" >
                        </form>
                    </div>
                </div>
                </a>
                    <hr>
                @endforeach

            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection