@extends('layouts.app')
@section('css')
@endsection
@section('title')
    Koszyk
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Koszyk</h2><br>
            </div>
            @if (Session::has('error'))
                <div class="col-12 alert alert-danger">
                        {!! Session::get('error') !!}

                </div>
            @endif

            @if(empty($cart))

            <div class="col-12 text-center">
                <h4>
                    Produkty pojawią się po dodaniu ich do koszyka :)
                </h4>
            </div>

            @else

            <table class="table table-striped table-edit-list" >
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Zdjęcie produktu</th>
                    <th scope="col" class="w-50">Przedmiot</th>
                    <th scope="col">Ilość</th>
                    <th scope="col">Cena</th>
                    <th scope="col">Akcje</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cart as $singleItem)
                    <tr>
                        <th scope="row" style="width: 10%;">
                            <img src="{{(asset(\App\Services\ProductService::STORAGE_IMAGES_PATH . $singleItem->options->image))}}" class="img-thumbnail" alt="">
                        </th>
                        <th>{{$singleItem->name}}</th>
                        <th>
                            <form action="{{route('product.updateCart', $singleItem->rowId, $singleItem->options->quantity)}}" method="POST" id="quantity-form{{$singleItem->id}}" class="qty-form" name="qtyform[]">
                                @csrf
                                <input name="quantity" type="number" value="{{$singleItem->qty}}" min="1" max="{{$singleItem->options->quantity}}" class="form-control form-control-sm">szt. <br>
                                (Max: {{$singleItem->options->quantity}})<br>
                            </form>
                        </th>
                        <th>{{$singleItem->price}} zł</th>
                        <th>
                            <form action="{{route('product.removeItemFromCart', $singleItem->rowId)}}" method="POST">
                                @csrf
                                <input type="submit" value="X" class="btn btn-danger">
                            </form>
                        </th>
                    </tr>
                @endforeach

                <tr>
                    <th class="text-right pr-5" colspan="4">W sumie: {{Cart::subtotal()}} zł</th>
                    <th>

                    </th>
                </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')
    <script>
        document.querySelectorAll('.qty-form').forEach(item => {
            item.addEventListener('change', (event) => {
               submitForm(item);
            })
        });

        function submitForm(element) {
            document.getElementById(element.id).submit();
        }

/*
 var x = document.getElementsByName("qtyform[]");
                console.log(x);

*/

    </script>
@endsection