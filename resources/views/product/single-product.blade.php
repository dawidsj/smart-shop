@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">

@endsection
@section('title')
    Produkty
@endsection

@section('content')
    <div class="container my-2">
        <a href="{{ url()->previous() }}" style="text-decoration: none; color: black">
            <i class="fas fa-angle-double-left"></i> Powrót
        </a>
    </div>
    <div class="container single-product py-4">
        <div class="row">
            <div class="col-4">
                <div class="row">
                @foreach($product->images as $image)
                    @if($image->order == 0)
                        <div class="col-12">
                            <a href="{{asset(\App\Services\ProductService::STORAGE_IMAGES_PATH . $image->name)}}" data-lightbox="roadtrip">
                                <img src="{{asset(\App\Services\ProductService::STORAGE_IMAGES_PATH . $image->name)}}" class="img-fluid img-thumbnail" alt="">
                            </a>
                        </div>
                    @else
                        <div class="col-4">
                            <a href="{{asset(\App\Services\ProductService::STORAGE_IMAGES_PATH . $image->name)}}" data-lightbox="roadtrip">
                                <img src="{{asset(\App\Services\ProductService::STORAGE_IMAGES_PATH . $image->name)}}" class="img-fluid img-thumbnail" alt="">
                            </a>
                        </div>
                    @endif
                @endforeach
                </div>
            </div>
            <div class="col-8">
                <h4>{{$product->name}}</h4><br>
                <h6>Liczba dostępnych sztuk: {{$product->quantity}}</h6><br>
                <h3>{{$product->price}} zł</h3> <br>
                <form action="{{route('product.addItemToCart', $product->id)}}" method="POST">
                    @csrf
                    <input type="submit" value="Dodaj do koszyka" class="btn btn-success btn-block" >
                </form>
            </div>
        </div>
    </div>
    @if($product->features->first())
    <div class="container single-product mt-3 py-3">
        <div class="row">
            <div class="col-12">
                <h4>Cechy produktu:</h4>
                @foreach($product->features as $feature)
                    <b>{{$feature->feature->name}}:</b> {{$feature->value}} <br>
                @endforeach
            </div>
        </div>
    </div>
    @endif
    <div class="container single-product mt-3 py-3">
        <div class="row">
            <div class="col-12">
                <h4>Opis produktu:</h4><br>
                {!!$product->description!!}<br>
            </div>
        </div>
    </div>


@endsection

@section('js')

    <script src="{{asset('js/lightbox-plus-jquery.js')}}"></script>

@endsection