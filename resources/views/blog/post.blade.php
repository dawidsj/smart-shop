@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{asset('css/post.css')}}">
    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">

    <style>
        body {
            background-image: url("{{asset(\App\Services\PostService::STORAGE_IMAGES_PATH . $post->image)}}");
            backdrop-filter: blur(15px);
        }
    </style>

@endsection
@section('title')
    Blog
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 card p-3 mb-5">
                <h4 class="mb-3">{{$post->topic}}</h4>
                {!!$post->content !!}
                <hr>
                    <a href="{{asset(\App\Services\PostService::STORAGE_IMAGES_PATH . $post->image)}}" rel="lightbox" data-lightbox="image-1" data-title="My caption">
                        <img src="{{asset(\App\Services\PostService::STORAGE_IMAGES_PATH . $post->image)}}" alt="" class="img-fluid py-3 mx-auto d-block photo" id="image-1">
                    </a>
                <hr>
                <span>Autor: {{$post->postAuthor->name}}</span>
                <span>Data dodania: {{$post->created_at}}</span>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{asset('js/lightbox-plus-jquery.js')}}"></script>

@endsection
