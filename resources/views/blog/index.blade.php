@extends('layouts.app')
@section('css')
@endsection
@section('title')
    Blog
@endsection

@section('content')

   <div class="container">
       <div class="container-macy">
           @foreach($posts as $post)
               <div class="single-post">
               <a href="{{route('blog.show', ['slug' => $post->slug])}}">
                    <img src="{{asset(\App\Services\PostService::STORAGE_IMAGES_PATH . $post->image)}}" alt="" class="img-fluid">
                    <h4 class="p-2 topic">{{$post->topic}}</h4>
                    <p class="p-2 teaser">{{$post->teaser}}</p>
               </a>
               </div>
           @endforeach
       </div>
   </div>

@endsection

@section('js')
    <script src="{{asset('js/macy.js')}}"></script>
    <script>
        const msnry = new Macy({
            container: '.container-macy',
            mobileFirst: true,
            columns: 1,
            breakAt: {
                576: 1,
                720: 2,
                960: 3,
            },
            margin : {
                x: 20,
                y: 20,
            }
        });
    </script>
@endsection