@extends('layouts.app')

@section('title')
    Panel
@endsection

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12 mx-auto" align="center">
            <h1>Lista wszystkich postów na blogu</h1>
        </div>
        <div class="col-12 my-3 py-3">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Tytuł</th>
                    <th scope="col">Id autora</th>
                    <th scope="col">Autor</th>
                    <th scope="col">Data dodania</th>
                    <th scope="col">Akcje</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <th scope="row">{{$post->id}}</th>
                        <td>{{$post->topic}}</td>
                        <td>{{$post->author}}</td>
                        <td>{{$post->postAuthor->name}}</td>
                        <td>{{$post->created_at}}</td>
                        <td>
                            <a href="{{route('panel.post.edit', ['id' => $post->id])}}" class="action-button"><i class="fas fa-edit"></i></a>

                            <i class="fas fa-trash-alt delete-modal action-button" data-value="{{$post->id}}" data-toggle="modal" data-target="#myModal"></i>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- MODAL --}}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Uwaga!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
            </div>
            <div class="modal-body">
                Czy na pewno chcesz usunąć ten post?<br> Operacja jest <b>nieodwracalna!</b>
            </div>
            <form method="POST" action="" data-action="{{route('panel.post.remove', ['id' => 'POST_ID'])}}" id="delete-form">
                <input type="hidden" name="_method" value="delete">
                @csrf
                <div class="modal-footer mb-0">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                    <button type="submit" class="btn btn-danger" name="delete_dividend" value="foo">Usuń post</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{--END OF THE MODAL--}}
@endsection
@section('js')
    <script src="{{asset('js/slug.js')}}"></script>

    <script>
        function sync() {
            const topic = document.getElementById('topic');
            const slug = document.getElementById('slug');
            slug.value = string_to_slug(topic.value);
            console.log(topic, slug);
        }
    </script>

    <script>
        $(document).ready(function (e) {
            $(document).on("click", ".delete-modal", function (e) {
                var delete_id = $(this).attr('data-value');
                $('button[name="delete_dividend"]').val(delete_id);
                var form = document.getElementById('delete-form')
                form.action = form.dataset.action.replace('POST_ID', delete_id);
                console.log(form.action);
            });
        });
    </script>
@endsection