@extends('layouts.app')

@section('title')
    Panel
@endsection

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12 mx-auto" align="center">
            @if(isset($post))
                <h1>Edycja</h1>
            @else
                <h1>Dodawnie nowego posta</h1>
            @endif
        </div>
        <div class="col-8 offset-2 my-3 py-3 new-post-form">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST" action="{{isset($post) ? route('panel.post.update', ['id' => $post->id]) : route('panel.post.store')}}" enctype="multipart/form-data">
                @if(isset($post))
                    <input type="hidden" name="id" value="{{$post->id}}">
                    <input type="hidden" name="_method" value="PUT">
                @endif

                @csrf
                <div class="form-group">
                    <label for="topic">Tytuł artykułu:</label>
                    <input type="text" name="topic" class="form-control col-12" id="topic" onkeyup="sync()" value="{{$post->topic ?? ''}}">
                </div>
                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input type="text" name="slug" class="form-control col-12" id="slug" value="{{$post->slug ?? ''}}">
                </div>
                <div class="form-group">
                    <label for="teaser">Teaser:</label>
                    <input type="text" name="teaser" class="form-control col-12" id="teaser" value="{{$post->teaser ?? ''}}">
                </div>
                <div class="form-group">
                    <label for="content">Treść artykułu:</label>
                    <textarea name="content" cols="30" rows="10" id="editor">
                        {{$post->content ?? ''}}
                    </textarea>
                </div>
                <div class="form-group">
                    <label for="image">Dodaj zdjęcie główne postu</label>
                    <input type="file" class="form-control-file" name="image" id="image">
                </div>
                <input type="submit" value="Submit" class="btn btn-success btn-block">
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script src="{{asset('js/slug.js')}}"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    </script>
    <script>
        function sync() {
            const topic = document.getElementById('topic');
            const slug = document.getElementById('slug');
            slug.value = string_to_slug(topic.value);
            console.log(topic, slug);
        }
    </script>
@endsection