@extends('layouts.app')

@section('title')
    Panel
@endsection

@section('content')
<div class="container">
    <div class="col-12 mx-auto" align="center">
        @if(isset($product))
            <h1>Edycja produktu o numerze: {{$product->id}}</h1>
        @else
            <h1>Dodawanie nowego produktu</h1>
        @endif
    </div>
    <div class="row">
        <div class="col-8 offset-2 my-3 py-3 new-post-form">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{isset($product) ? route('panel.product.update', ['id' => $product->id]) : route('panel.product.store')}}" enctype="multipart/form-data">
                @if(isset($product))
                    <input type="hidden" name="id" value="{{$product->id}}">
                    <input type="hidden" name="_method" value="PUT">
                @endif
                @csrf
                <div class="form-group">
                    <label for="name"><h5>Nazwa produktu:</h5></label>
                    <input type="text" name="name" class="form-control col-12" id="name" onkeyup="sync()" value="{{$product->name ?? ''}}">
                </div>
                <div class="form-group">
                    <label for="slug"><h5>Slug:</h5></label>
                    <input type="text" name="slug" class="form-control col-12" id="slug" value="{{$product->slug ?? ''}}">
                </div>
                <div class="form-group">
                    <label for="description"><h5>Opis produktu:</h5></label>
                    <textarea type="text" name="description" class="form-control col-12" id="editor">{{ $product->description ?? ''}}</textarea>
                </div>

                {{-- FEATURES --}}
                {{--     @if(is_null($product->features->first()))--}}
                <div class="form-group">
                    <h5>Podaj wszystkie cechy jakie ma twój produkt:</h5>
                    <h6>Aby dodać cechę naciśnij +</h6>

                    <button type="button" name="add" id="add" class="btn btn-block btn-success" onclick="copyFeatureSelect()">+</button>

                    <table class="table table-bordered" id="dynamic-field">
                        <tr style="display: none">
                            <td>
                                <select name="features[]" id="feature" class="custom-select" disabled>
                                    <option selected>Wybierz ceche produktu:</option>

                                    @foreach($features as $feature)
                                        <option value="{{$feature->id}}">{{$feature->name}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input type="text" name="values[]" placeholder="Podaj wartość cechy [x]" class="form-control" disabled>
                            </td>
                            <td>
                                <button type="button" name="add" id="add" class="btn btn-block btn-danger" onclick="deleteFeatureSelect(this)">X</button>
                            </td>
                        </tr>

                        @if(isset($product))
                            @foreach($product->features as $feature)
                                <tr style="display: table-row">
                                    <td>
                                        <select name="features[]" id="feature" class="custom-select" >

                                            <option value="{{$feature->feature->id}}">{{$feature->feature->name}}</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="values[]" placeholder="Podaj wartość cechy [x]" class="form-control" value="{{$feature->value}}">
                                    </td>
                                    <td>
                                        <button type="button" name="add" id="add" class="btn btn-block btn-danger" onclick="deleteFeatureSelect(this)">X</button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
                <div class="form-group">
                    <label for="price"><h5>Cena produktu:</h5></label>
                    <input type="number" step="0.01" name="price" class="form-control col-12" id="price" value="{{$product->price ?? ''}}" >
                </div>
                <div class="form-group">
                    <input type="checkbox" name="active" value="1" class="form-check-label" id="active"@if(isset($product)) @if($product->active == 1) checked @endif @endif>
                    <label for="active"><h5>Czy produkt jest dostępny?</h5></label>
                </div>
                <div class="form-group">
                    <label for="quantity"><h5>Podaj ilość posiadanego produktu:</h5></label>
                    <input type="number" name="quantity" class="form-control col-12" id="quantity" value="{{$product->quantity ?? ''}}">
                </div>
                <div class="form-group">
                    <label for="image"><h5>Zdjęcie artykułu</h5></label>
                    <input type="file" class="form-control-file" name="image[]" id="image" multiple>
                </div>
                <input type="submit" value="Submit" class="btn btn-success btn-block">
            </form>
        </div>
    </div>
</div>

@endsection
@section('js')
    <script src="{{asset('js/slug.js')}}"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );

        function sync() {
            const name = document.getElementById('name');
            const slug = document.getElementById('slug');
            slug.value = string_to_slug(name.value);
            console.log(name, slug);
        }

        // Dynamicly features fields

        function copyFeatureSelect() {
            const row = document.getElementById('dynamic-field').firstElementChild.firstChild;
            const clone = row.cloneNode(true);

            clone.firstElementChild.firstElementChild.disabled = false;
            clone.children[1].lastElementChild.disabled = false;
            clone.style.display = 'table-row';

            document.getElementById('dynamic-field').append(clone);
        }

        function deleteFeatureSelect(element) {
            element.parentElement.parentElement.remove();
        }


    </script>
@endsection