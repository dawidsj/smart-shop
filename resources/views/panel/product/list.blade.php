@extends('layouts.app')

@section('title')
    Panel
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 mx-auto" align="center">
                <h1>Lista wszystkich <b>produktów</b></h1>
            </div>
            <div class="col-12 my-3 py-3">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <table class="table table-striped table-edit-list" >
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Zdjęcie</th>
                        <th scope="col">Nazwa</th>
                        <th scope="col">Dostępne szt.</th>
                        <th scope="col">Aktywny?</th>
                        <th scope="col">Cechy</th>
                        <th scope="col">Akcje</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            <tr>
                                <th scope="row">{{$product->id}}</th>
                                <td style="width: 15%;">
                                    <img src="{{asset(\App\Services\ProductService::STORAGE_IMAGES_PATH . $product->images->first()->name)}}" class="img-thumbnail" alt="">
                                </td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->quantity}}</td>
                                <td>
                                    @if($product->active == 1)
                                        <span style="color:green;"><b>Tak</b></span>
                                    @else
                                        <span style="color:red;"><b>Nie</b></span>
                                    @endif
                                </td>
                                <td>
                                    @foreach($product->features as $feature)
                                    <b>{{$feature->feature->name . ': '}}</b> {{$feature->value}}<br>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{route('panel.product.edit', ['id' => $product->id])}}" class="action-button"><i class="fas fa-edit"></i></a>

                                    <i class="fas fa-trash-alt delete-modal action-button" data-value="{{$product->id}}" data-toggle="modal" data-target="#myModal"></i>
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- MODAL --}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Uwaga!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                </div>
                <div class="modal-body">
                    Czy na pewno chcesz usunąć ten post?<br> Operacja jest <b>nieodwracalna!</b>
                </div>
                <form method="POST" action="" data-action="{{route('panel.product.remove', ['id' => 'PRODUCT_ID'])}}" id="delete-form">
                    <input type="hidden" name="_method" value="delete">
                    @csrf
                    <div class="modal-footer mb-0">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-danger" name="delete_dividend" value="foo">Usuń post</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('js/slug.js')}}"></script>


    <script>
        $(document).ready(function (e) {
            $(document).on("click", ".delete-modal", function (e) {
                var delete_id = $(this).attr('data-value');
                $('button[name="delete_dividend"]').val(delete_id);
                var form = document.getElementById('delete-form')
                form.action = form.dataset.action.replace('PRODUCT_ID', delete_id);
                console.log(form.action);
            });
        });
    </script>
@endsection