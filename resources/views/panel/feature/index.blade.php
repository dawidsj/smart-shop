@extends('layouts.app')

@section('title')
    Panel
@endsection

@section('content')
<div class="container">
    <div class="col-12 mx-auto" align="center">
            <h1>Dodawanie nowej cechy produktu</h1>
        </div>
    <div class="row">

        <div class="col-8 offset-2 my-3 py-3 new-post-form">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{route('panel.feature.store')}}">
                @csrf
                <div class="form-group">
                    <label for="name"><h5>Dodaj nową cechę:</h5></label>
                    <input type="text" name="name" class="form-control col-12" id="name">
                </div>
                <input type="submit" value="Dodaj" class="btn btn-success">
            </form>
            <hr>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Lista cech:</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($features as $feature)
                        <tr>
                            <td>{{$feature->name}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>

        </div>
    </div>
</div>
@endsection
@section('js')

@endsection