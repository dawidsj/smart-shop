@extends('layouts.app')

@section('title')
    Panel
@endsection

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12 mx-auto" align="center">
            <h1>Witaj w panelu {{\Illuminate\Support\Facades\Auth::user()->name}} :)</h1>
            <h2>Co chcesz zrobić?</h2>
            <hr>
        </div>
        <div class="col-12 mx-auto" align="center">
            <h2>Blog</h2>
            <h2><a href="{{route('panel.post.create')}}" class="btn btn-block btn-dark">Dodaj nowy post</a></h2>
            <h2><a href="{{route('panel.post.editList')}}" class="btn btn-block btn-dark">Edytuj już istniejący post</a></h2>
            <hr>
            <h2>Sklep</h2>
            <h2><a href="{{route('panel.product.create')}}" class="btn btn-block btn-dark">Dodaj nowy produkt</a></h2>
            <h2><a href="{{route('panel.product.editList')}}" class="btn btn-block btn-dark">Edytuj już istniejący produkt</a></h2><br>

            <h2><a href="{{route('panel.feature.create')}}" class="btn btn-block btn-dark">Dodaj nową cechę produktu</a></h2>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection