@component('mail::message')
#  Numer sprawy: {{\App\Models\ContactEmail::query()->get()->last()->id}}

# Temat: {{request()->topic}}
# Email: {{request()->email}}

{!!request()->msgContent!!}

Dziękujemy za kontakt, {{request()->name}}<br>
{{ config('app.name') }}
@endcomponent
