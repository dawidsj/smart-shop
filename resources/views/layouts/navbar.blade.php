<header class="mb-4">
    <nav class="navbar navbar-expand navbar-login-panel my-0">
        <div class="container">
            <button class="navbar-toggler navbar-dark d-block d-md-none" type="button" data-toggle="collapse" data-target="#collapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a href="{{route('index')}}" class="navbar-brand navbar-dark d-block d-md-none mx-auto" style="font-family: 'Lobster', cursive;">
                <i class="fas fa-shopping-basket header-logo"></i>
                <span class="header-text">Smart Shop</span>
            </a>
            <ul class="navbar-nav ml-auto list-inline">
                @if(\Illuminate\Support\Facades\Auth::check())
                <li class="nav-item d-inline">
                    <span class="nav-link d-block"><a href="{{route('panel')}}"> Witaj {{\Illuminate\Support\Facades\Auth::user()->name}}</a></span>
                </li>
                <li class="nav-item list-inline-item">
                    <a href="{{route('auth.logout')}}" class="nav-link">
                       Wyloguj się <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>
                @else
                <li class="nav-item list-inline-item">
                    <a href="{{route('auth')}}" class="nav-link">
                        Logowanie/Rejestracja <i class="fas fa-sign-in-alt"></i>
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </nav>



    <div class="navbar navbar-expand-md navbar-dark navbar-buttons-panel my-0 py-0 pt-1" id="lower-navbar">
        <div class="container">
            <a href="{{route('index')}}" class="navbar-brand navbar-dark d-none d-md-block" style="font-family: 'Lobster', cursive;">
                <i class="fas fa-shopping-basket header-logo"></i>
                <span class="header-text">Smart Shop</span>
            </a>
            <div class="collapse navbar-collapse" id="collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="{{route('index')}}" class="nav-item nav-link navbar-button" title="Strona główna">
                            Strona główna
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-item nav-link navbar-button disabled">
                            <del>Promocje</del>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('product.list')}}" class="nav-item nav-link navbar-button">
                            Sklep
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('blog.blog')}}" class="nav-item nav-link navbar-button">
                            Blog
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('contact.form')}}" class="nav-item nav-link navbar-button" >
                            Kontakt
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('product.cart')}}" class="nav-link">
                            <i class="fas fa-shopping-basket"></i> <sup>({{Cart::count()}})</sup>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>



