const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .copy('node_modules/macy/dist/macy.js', 'public/js')
    .copy('node_modules/lightbox2/dist/js/lightbox-plus-jquery.js', 'public/js')
    .copy('node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js', 'public/js')
    .copy('resources/assets/images/', 'public/images/')
    .copy('resources/js/slug.js', 'public/js');

mix.sass('resources/scss/app.scss', 'public/css/app.css')
    .sass('resources/scss/post.scss', 'public/css/post.css')
    .copy('node_modules/lightbox2/dist/css/lightbox.css', 'public/css');

mix.copy('node_modules/lightbox2/dist/images/close.png', 'public/images')
    .copy('node_modules/lightbox2/dist/images/loading.gif', 'public/images')
    .copy('node_modules/lightbox2/dist/images/next.png', 'public/images')
    .copy('node_modules/lightbox2/dist/images/prev.png', 'public/images');


