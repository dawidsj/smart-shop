<?php

use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BlogPostsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\FeatureController;
use App\ImageFileName\ImageManagerHelper;
use \App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');


// PANEL

Route::group(['prefix' => 'panel', 'as' => 'panel', 'middleware' => ['auth']], function () {
    Route::get('', function () {return view('panel.index');});

    Route::group(['prefix' => 'post', 'as' => '.post.'], function () {
        Route::get('create', [BlogPostsController::class, 'create'])->name('create');
        Route::get('edit/list', [BlogPostsController::class, 'listPosts'])->name('editList');
        Route::get('edit/{id}', [BlogPostsController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [BlogPostsController::class, 'update'])->name('update');
        Route::post('store', [BlogPostsController::class, 'store'])->name('store');
        Route::delete('remove/{id}', [BlogPostsController::class, 'remove'])->name('remove');
    });


    Route::group(['prefix' => 'product', 'as' => '.product.'], function () {
        Route::get('create', [ProductsController::class, 'create'])->name('create');
        Route::post('store', [ProductsController::class, 'store'])->name('store');
        Route::get('edit/list', [ProductsController::class, 'editlist'])->name('editList');
        Route::get('edit/{id}', [ProductsController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [ProductsController::class, 'update'])->name('update');
        Route::delete('remove/{id}', [ProductsController::class, 'remove'])->name('remove');
    });

    Route::group(['prefix' => 'feature', 'as' => '.feature.'], function () {
        Route::get('create', [FeatureController::class, 'create'])->name('create');
        Route::post('store', [FeatureController::class, 'storeFeature'])->name('store');
    });
});

// AUTH
Route::group(['prefix' => 'auth', 'as' => 'auth'], function () {
    Route::group(['middleware' => 'logged_in'], function () {
        Route::get('', function () {return view('auth.index');});

        Route::post('login', [LoginController::class, 'login'])->name('.login');
        Route::post('register', [RegisterController::class, 'register'])->name('.register');
    });

    Route::get('logout', [LoginController::class, 'logout'])->name('.logout');
});

// BLOG
Route::group(['prefix' => 'blog', 'as' => 'blog.'], function() {
    Route::get('', [BlogPostsController::class, 'renderBlog'])->name('blog');
    Route::get('{slug}', [BlogPostsController::class, 'show'])->name('show');

});

//PRODUCTS
Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
    Route::get('list', [ProductsController::class, 'list'])->name('list');
    Route::get('cart', [ProductsController::class, 'cart'])->name('cart');
    Route::post('cart/add/{id}', [ProductsController::class, 'addItemToCart'])->name('addItemToCart');
    Route::post('cart/update/{id}', [ProductsController::class, 'updateCart'])->name('updateCart');
    Route::post('cart/remove/{id}', [ProductsController::class, 'removeItemFromCart'])->name('removeItemFromCart');

    Route::get('preview/{slug}', [ProductsController::class, 'show'])->name('show');
});

//CONTACT
Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
    Route::get('form', [ContactController::class, 'form'])->name('form');
    Route::post('send/email', [ContactController::class, 'sendContactEmail'])->name('sendEmail');

});

Route::get('/emailtest', function () {
    Mail::to('email@email.com')->send(new ContactEmail());

});





Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
